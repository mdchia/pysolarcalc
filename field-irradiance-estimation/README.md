# Field Irradiance Estimation

Script to use pysolarcalc for estimating average daily sunlight (as photosynthetic photon flux density (PPFD), in umol/m^2/s) at local noon across multiple sites. Should work well enough for relative light differences on similar vegetative environments (e.g. crop fields, grassland). 

The usual caveats from pysolarcalc estimation remain; in addition, local noon being close to peak solar radiation may be particularly unreliable in some timezones.

In addition to requiring pysolarcalc to be installed, intermediate processing uses R via [RStudio](https://posit.co/download/rstudio-desktop/).

Example usage steps:

1. Fill in csv2solarcalc_inputs.csv with your target sites.
2. Run `python csv2solarcalc_inputs.csv > solarcalc.sh`
3. Run `solarcalc.sh`
4. Copy the resulting .csv files into the `data/` directory.
5. Run `process_spectral_predictions.R`.
6. This should give you a `irradiance_summary.csv` with the results.
