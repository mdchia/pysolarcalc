library(tools)
library(dplyr)

setwd(dirname(rstudioapi::getSourceEditorContext()$path)) # only works in RStudio, set manually otherwise 

dirpath <- "./data"

# reset if rerunning source in RStudio
if (exists("all.data")){
  rm(all.data)
}

for (filename in list.files(dirpath, full.names = TRUE)){
  this.file <- read.csv(filename, sep = "\t")
  this.file$sample <- file_path_sans_ext(basename(filename))
  # Create the first df if no data exist yet
  if (!exists("all.data")){
    all.data <- data.frame(this.file)
  }
  # if df exists, then append
  if (exists("all.data")){
    all.data <- unique(rbind(all.data, this.file))
  }
  rm(this.file)
}

all.data <- all.data[all.data$total_solar != 0,]

# colnames should be [datetime,datetime-sim,temperature,humidity,total_solar,1,sample]
df <- all.data[, c(1,5,7)]
df$sample <- as.factor(df$sample)

summary <- group_by(df, sample) %>%
  summarise(avg=mean(total_solar))

options(digits = 2)
summary$avg <- sprintf("%.2f", summary$avg)

write.csv(summary, file = "irradiance_summary.csv", row.names = FALSE)