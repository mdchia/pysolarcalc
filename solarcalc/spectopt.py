# Copyright (c) 2018-2019 Kevin Murray <foss@kdmurray.id.au>
# Copyright (c) 2018 Gareth Dunstone

# This Source Code Form is subject to the terms of the Mozilla Public License,
# v. 2.0. If a copy of the MPL was not distributed with this file, you can
# obtain one at http://mozilla.org/MPL/2.0/.


import numpy as np
from scipy.interpolate import interp1d, UnivariateSpline
from scipy import optimize
from scipy.spatial import distance
import pandas as pd

from sys import stderr


class Spectrum(object):
    """Holds a spectral density curve, in Watts per m2 per nm, also integrate and interpolate"""

    def __init__(self, wavelengths, values, watts=True):
        self.wavelengths = np.array(wavelengths).astype(float)
        self.values = np.array(values).astype(float)
        self.watts = watts

    @classmethod
    def read(cls, fileorpath):
        df = pd.read_csv(fileorpath)
        return cls(df.values[:, 0], df.values[:,1])

    def interpolated(self, method='linear'):
        '''Create a function to interpolate spectrum to new wavelengths'''
        def interpfn(x):
            minwl = min(self.wavelengths)
            maxwl = max(self.wavelengths)
            wl = self.wavelengths
            vals = self.values
            if any(x < minwl):
                newwl = np.arange(min(x), minwl)
                wl = np.append(newwl, wl)
                vals = np.append(np.zeros_like(newwl), vals)
            if any(x > maxwl):
                newwl = np.arange(maxwl, max(x)+1)
                wl = np.append(wl, newwl)
                vals = np.append(vals, np.zeros_like(newwl))
            interp = interp1d(wl, vals, kind=method)
            return interp(x)
        return interpfn

    def spline(self, k=1, s=0):
        '''Create a function to interpolate spectrum to new wavelengths with a spline'''
        return UnivariateSpline(self.wavelengths, self.values, k=k, s=0)

    def one_nm(self, method='linear'):
        '''Gives an interpolated version of self with 1nm bands'''
        wl = np.arange(min(self.wavelengths), max(self.wavelengths)+1, dtype=int)
        return Spectrum(wl, self.interpolated(method)(wl))

    def _p2w2p(self, left: float=None, right: float=None, to_watts=True):
        """Helper function. Converts between photon and watt representations of spectra. Don't call directly."""
        from scipy.constants import c, h, Avogadro
        if left is None:
            left = min(self.wavelengths)
        left = max(left, min(self.wavelengths))
        if right is None:
            right = max(self.wavelengths)
        right = min(right, max(self.wavelengths))

        wl = np.arange(left, right+1, dtype=float)
        if self.watts:
            if to_watts:
                # no conversion needed
                return self
            wsqm = self.interpolated()(wl)
            ep = h * (c / (wl/1e9)) # e = hcL^-1; w/ L = wl in meters 
            n = (wsqm / ep) / Avogadro
            return Spectrum(wl, n, watts=False)
        else:
            if not to_watts:
                # no conversion needed
                return self
            E = self.interpolated()(wl)  # mole photons m-2 s-1 /nm
            ep = h * (c / (wl/1e9)) # e = hcL^-1; w/ L = wl in meters 
            wsqm = E * Avogadro * ep
            return Spectrum(wl, wsqm, watts=True)

    def photons(self, left: float=None, right: float=None):
        """Returns the current spectrum encoded as mol photon per m2 per second per nm

        :param left: Left-hand wavelength of spectrum nm
        :param right: Right-hand wavelength of spectrum nm
        :rtype: Spectrum object
        """
        return self._p2w2p(left, right, to_watts=False)

    def watts(self, left: float=None, right: float=None):
        """Returns the current spectrum encoded as watts per m2 per second per nm

        :param left: Left-hand wavelength of spectrum nm
        :param right: Right-hand wavelength of spectrum nm
        :rtype: Spectrum object
        """
        return self._p2w2p(left, right, to_watts=True)


    def par(self, left: float = 400, right: float = 700):
        """Calculates PAR as integrated uE between `left` and `right` (default 400-700nm)
        
        :param left: Left-hand wavelength of spectrum nm
        :param right: Right-hand wavelength of spectrum nm
        :return: Photosynthetically active radiation in micromol per m2 per second (uE)
        :rtype: float
        """
        par = sum(self.photons(left, right).values)
        return par * 1e6  #  1e6 is to make it uE, not E

    def banded_integral(self, bands):
        '''Gives the integral within each band, where bands are (lhs, rhs)'''
        s = self.spline()
        integr = []
        for band in bands:
            integr.append(s.integral(*band))
        return integr

    def __getitem__(self, item):
        if isinstance(item, slice):
            keep = np.logical_and(self.wavelengths >= item.start, self.wavelengths <= item.stop)
            return Spectrum(self.wavelengths[keep], self.values[keep])


class CostFunc(object):
    '''Base class of cost functions, designed to be used with Light.optimise_settings'''

    def __call__(self, weights, light, desired, watts=False):
        got = light.light_output(weights, wavelengths=desired.wavelengths)
        if not watts:
            got = got.photons()
            desired = desired.photons()
        cost = self._cost(got, desired)
        return cost

    
class SimpleCost(CostFunc):
    '''Simple sum of distances across all '''
    def __init__(self, cost=None):
        self.cost = cost

    def _cost(self, x, y):
        x = x.one_nm()
        y = y.one_nm()
        if self.cost is None:
            dist =  np.sum(np.abs(x.values - y.values))
        else:
            dist =  np.sum(np.abs(x.values - y.values) * cost.one_nm().values)
        return dist


class BandCost(CostFunc):
    '''Cost defined by the differnce in banded integrals along wavelengths, possibly weighted'''
    def __init__(self, bands=None, weights=None):
        if bands is None:
            bands = [(x, x+100) for x in range(300, 700, 100)]
        if weights is None:
            weights = np.array([1 for x in bands])
        self.bands = bands
        self.weights = weights

    def _cost(self, x, y):
        x = np.array(x.banded_integral(bands=self.bands))
        y = np.array(y.banded_integral(bands=self.bands))
        dist =  np.sum(np.abs(x - y) * self.weights)
        return dist


class Light(object):
    """
    Loads a CSV describing a light source, interpolates the output, and
    allows extraction to custom domains and optimisation to other spectra
    """

    def __init__(self, filename, interpolation='linear'):
        df = pd.read_csv(filename)
        self.channels = {}
        self.wavelengths = df.values[:, 0]
        for chan in df.columns[1:]:
            values = df[chan]
            self.channels[chan] = Spectrum(self.wavelengths, values)

    def __len__(self):
        return len(self.channels)

    def fit_wavelengths(self, wavelengths=None, minwl=None, maxwl=None, step=1):
        '''Interpolate each channel to be defined over certian wavelengths'''
        if wavelengths is None and minwl is None and maxwl is None:
            #raise ValueError("Either wavelengths or maxwl and minwl must be given")
            wavelengths = self.wavelengths
        if wavelengths is None:
            wavelengths = np.arange(minwl, maxwl, step)
        return np.vstack([ch.interpolated()(wavelengths)
                          for ch in self.channels.values()]).T

    def light_output(self, channel_settings, wavelengths=None):
        '''Gives the total output of the light unit over wavelengths given channel_settings'''
        if wavelengths is None:
            wavelengths = self.wavelengths
        outputs = self.fit_wavelengths(wavelengths)
        return Spectrum(wavelengths, np.dot(outputs, channel_settings))

    def max_output(self):
        return self.light_output(np.ones(len(self.channels)))

    def optimise_settings(self, desired: Spectrum, cost_function=SimpleCost(), watts=False):
        '''Optimise channel settings aiming for desired, using cost_function

        :param desired:  The output spectrum we wish to obtain from lights, as Spectrum() object
        :param cost_function:  A cost function (see SimpleCost, BandCost, etc)
        :param watts: Optmise spectra in units of W per m2 per second. If false, optmise in units of photons (uE)
        :return:  The optimal channel settings on a 0-1 scale.
        :rtype: np.ndarray with shape (n_channels, )
        '''
        initial = np.ones(len(self.channels))/2
        bounds = [(0.,1.)]*len(initial)
        opt = optimize.minimize(cost_function, initial, (self, desired, watts),
                                options={"maxiter": 10000}, bounds=bounds)
        return opt.x
