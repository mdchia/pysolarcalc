[![pipeline status](https://gitlab.com/mdchia/pysolarcalc/badges/master/pipeline.svg)](https://gitlab.com/mdchia/pysolarcalc/-/commits/master)

# pysolarcalc

A python port of SolarCalc by Kurt Spokas

Follows formulae largely from [Spokas and Forcella (2006)](https://pubag.nal.usda.gov/catalog/1910) and [this NREL tech report](https://rredc.nrel.gov/solar/pubs/spectral/model/section3.html)

## Installation

With pip:

```bash
pip install -r requirements.txt && ./setup.py install
```

With conda:

```bash
conda env create -f dev-environment.yml && ./setup.py install
```

## Usage - sunlight simulation

```bash
pysolarcalc --place $lat $lon $elev \
    --light data/spectra/growtainer-wm2.csv  \
    --start 2019-01-01 \
    --end 2019-12-31 \
    --chamber-start 2020-01-01 \
    --scale-factor 0.25  # see below
```

* `lat/lon` are in decimal degrees.
* Elevation is in metres above sea level. Usually can be set to just sea level unless simulating for high altitude (>2000m).
* `--light` is the intended lighting spectra file (same as in sunlight simulation mode)
* Start and end dates are for the source time period being calculated (in this case a whole year, from southern hemisphere winter and back)
* `--chamber-start` is when the date of the experiment begins, used for the output.
* `--scale-factor` is a multiplier that accounts for the fact that a chamber
  cannot produce the quantity of light the sun does, and allows one to optimise
  spectra assuming that the sun was `scale_factor` times as bright. Defaults to
  0.5, which is about right for our high light chambers. It should be set to
  approximately `max(chamber par) / max(sun)`.

## Usage - custom spectra simulation

```bash
pysolarcalc-spectraloptim --target-spectrum cool_white_fluoro.csv \
    --light data/spectra/growtainer-wm2.csv  \
    --normalize none
    -o output.csv
```

* `--target_spectrum` takes a CSV file containing the target spectra in solarcalc format.
* `--light` is the intended lighting spectra file (same as in sunlight simulation mode)
* `--normalize` normalises the output targets to:
 * have the brightest light channel at 100% (`maxonechannel`)
 * have the light channels set as a %, i.e. sum to 100% (`percent`)
 * apply no normalisation (`none`)
* `-o/--output` for the output filename (also a CSV file)

## Adding new lights

pysolarcalc takes spectra as a CSV file; see [this file](data/spectra/growtainer-white-high-light-wm2.csv) as an example measured spectra.

| `wavelength` (constant)       | First channel label (e.g. `Red`) | Second channel label | (more channels as needed per light) |
|-------------------------------|----------------------------------|----------------------|-------------------------------------|
| 400                           | (measured light values in W/m^2) | ...                  | ...                                 |
| ... (other wavelengths in nm) | ...                              | ...                  | ...                                 |
| 700                           | ...                              | ...                  | ...                                 |
