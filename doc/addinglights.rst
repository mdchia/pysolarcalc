Adding new lights
=================

pysolarcalc takes spectra as a CSV file; see
`this file <data/spectra/growtainer-white-high-light-wm2.csv>`_ as an example
measured spectra.

+---------------------------------+------------------------------------+----------------------+-------------------------------------+
| ``wavelength`` (constant)       | First channel label (e.g. ``Red``) | Second channel label | (more channels as needed per light) |
+=================================+====================================+======================+=====================================+
| 400                             | (measured light values in W/m^2)   | ...                  | ...                                 |
+---------------------------------+------------------------------------+----------------------+-------------------------------------+
| ... (other wavelengths in nm)   | ...                                | ...                  | ...                                 |
+---------------------------------+------------------------------------+----------------------+-------------------------------------+
| 700                             | ...                                | ...                  | ...                                 |
+---------------------------------+------------------------------------+----------------------+-------------------------------------+

If measuring spectra with a SpectraPen SP100 or similar, an
`example conversion script <https://gitlab.com/appf-anu/pysolarcalc/-/blob/master/data/spectrapen_processor/spectrapen_converter.py>`_
has been written to convert the formats.
