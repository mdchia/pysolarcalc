Installation
============

With pip:

.. code-block:: bash

   pip install -r requirements.txt && ./setup.py install


With conda:

.. code-block:: bash

   conda env create -f dev-environment.yml && ./setup.py install


