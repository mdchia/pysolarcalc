Usage
=====

Sunlight simulation
-------------------


.. code-block:: bash

   pysolarcalc --place $lat $lon $elev \
       --light data/spectra/growtainer-wm2.csv  \
       --start 2019-01-01 \
       --end 2019-12-31 \
       --chamber-start 2020-01-01 \
       --scale-factor 0.25 \
       --output output.csv

- ``lat/lon`` are in decimal degrees.
- Elevation is in metres above sea level. Usually can be set to just sea level
  unless simulating for high altitude (>2000m).
- ``--light`` is the intended lighting spectra file (same as in sunlight
    simulation mode)
- Start and end dates are for the source time period being calculated (in this
    case a whole year, from southern hemisphere winter and back)
- ``--chamber-start`` is when the date of the experiment begins, used for the
  output.
- ``--scale-factor`` is a multiplier that accounts for the fact that a chamber
  cannot produce the quantity of light the sun does, and allows one to optimise
  spectra assuming that the sun was ``scale_factor`` times as bright. Defaults to
  0.5, which is about right for our high light chambers. It should be set to
  approximately ``max(chamber par) / max(sun)``.
- ``-o/--output`` for the output filename (can be CSV or XLSX)

Custom spectra simulation
-------------------------

.. code-block:: bash

   pysolarcalc-spectraloptim --target_spectrum cool_white_fluoro.csv \
       --light data/spectra/growtainer-wm2.csv  \
       --normalize none \
       -o output.csv

- ``--target_spectrum`` takes a CSV file containing the target spectra in
  solarcalc format.
- ``--light`` is the intended lighting spectra file (same as in sunlight
    simulation mode)
- ``--normalize`` normalises the output targets to:
 - have the brightest light channel at 100% (``maxonechannel``)
 - have the light channels set as a %, i.e. sum to 100% (``percent``)
 - apply no normalisation (``none``)
- ``-o/--output`` for the output filename (also a CSV file)
